package runningprogram;


import cli.GenBankReaderCliOptions;
import specifier.ModelSpecifier;


/**
 * Executes the whole program by creating options and validating them if the
 * options are incorrectly filled in the program stops.
 */
public class Main {
    public static void main(String[] args) {
        GenBankReaderCliOptions argumentParser = new GenBankReaderCliOptions(args);
        if (!argumentParser.isInputIsCorrect()){
            return;
        }
        String chosenOption = argumentParser.getSelectedOption();
        String chosenGenbankFile = argumentParser.getInfileOption();
        String givenInputValue = argumentParser.getSelectedOptionInputValue();
        new ModelSpecifier(chosenGenbankFile, chosenOption, givenInputValue);


    }
}
