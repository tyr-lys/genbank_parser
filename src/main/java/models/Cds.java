package models;

import dnasequenceutilities.DnaUtilities;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Used for collecting CDS superTags from a genBank File.
 */
public class Cds extends SuperTag {
    private ArrayList<Integer> cdsPrintOrderValues;
    private HashMap<Integer,String> cdsDirectionsHashMap;
    private HashMap<Integer,String> cdsFoundNamesHashMap;
    private HashMap<Integer,Integer> cdsStartPositionsHashMap;
    private HashMap<Integer,Integer> cdsStopPositionsHashMap;


    /**
     * Inherits from SuperTag and gets all the values it needs to form a CDS object.
     * @param superTagReader Buffered Reader that reads the genBank.
     * @param targetSuperTagName String of the cds that is looked for.
     * @param tagDeterminingString String of the subTag that should be used to look for name of the cds.
     * @param requestedSuperTag String of the superTag that is looked for.
     */
    public Cds(BufferedReader superTagReader, String targetSuperTagName, String tagDeterminingString, String requestedSuperTag) {
        super(superTagReader, targetSuperTagName, tagDeterminingString, requestedSuperTag);
        cdsPrintOrderValues = new ArrayList<>(super.getSuperTagPrintOrderValues());
        cdsDirectionsHashMap = new HashMap<>(super.getDirectionsHashMap());
        cdsFoundNamesHashMap = new HashMap<>(super.getFoundSuperTagNamesHashMap());
        cdsStartPositionsHashMap = new HashMap<>(super.getStartPositionsHashMap());
        cdsStopPositionsHashMap = new HashMap<>(super.getStopPositionsHashMap());
    }

    @Override
    public String toString() {
        ArrayList<String> masterString = new ArrayList<>();
        String cdsSequence;
        String stringResultTemplate = ">CDS %s Sequence\n%s\n\n";
        for (Integer cdsOrderValue: cdsPrintOrderValues){
            if(cdsDirectionsHashMap.get(cdsOrderValue).equals("R")){
                cdsSequence = DnaUtilities.convertNucleotidesToAminoAcids(
                        DnaUtilities.dnaComplementMaker(superTagOrigin.originSubstring(
                                cdsStartPositionsHashMap.get(cdsOrderValue),
                                        cdsStopPositionsHashMap.get(cdsOrderValue))).toUpperCase());
            } else{
                cdsSequence = DnaUtilities.convertNucleotidesToAminoAcids(
                        superTagOrigin.originSubstring(
                                cdsStartPositionsHashMap.get(cdsOrderValue),
                                cdsStopPositionsHashMap.get(cdsOrderValue)).toUpperCase());
            }
            masterString.add(String.format(stringResultTemplate,
                    cdsFoundNamesHashMap.get(cdsOrderValue),
                    sequenceLengthManager(cdsSequence)));
        }
        return String.join("",masterString);
    }
}
