package models;

import java.io.BufferedReader;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;


/**
 * Methods for making a summary from the genBank file.
 */
public class Summary {
    private String fileName;
    private String organism;
    private String accession;
    private String sequenceLength;
    private int numberOfGenes = 0;
    private double forwardReverseBalance = 0.0;
    private int numberOfCDS = 0;
    private HashMap<String, Integer> tagHashMap = new HashMap<>();
    private HashMap<String,Integer> forwardReverseHashMap = new HashMap<>();


    /**
     * Makes a summary of the GenBank file.
     * @param genBankFileName String of the genBank file name that is used to display which file is used.
     * @param summaryReader Buffered reader that reads the genBank file.
     */
    public Summary(String genBankFileName ,BufferedReader summaryReader) {
        lineProcessing(summaryReader);
        fileName = genBankFileName;
        numberOfGenes = tagHashMap.get("gene");
        forwardReverseBalance = calculateForwardReverseBalance();
        numberOfCDS = tagHashMap.get("CDS");

    }

    /**
     * Reads the genBank file and collects information regarding the file.
     * @param fileReader Buffered reader that reads the genbank file.
     */
    private void lineProcessing(BufferedReader fileReader){
        forwardReverseHashMap.put("forward",0);
        forwardReverseHashMap.put("reverse",0);
        String fileLine;
        try {
            while ((fileLine = fileReader.readLine()) != null) {
                if (fileLine.startsWith("     CDS")){
                    tagHashMapUpdater("CDS");
                } else if (fileLine.startsWith("     gene")) {
                    tagHashMapUpdater("gene");
                    forwardReverseHashMapUpdater(fileLine);
                }else if (fileLine.startsWith("LOCUS")) {
                    collectInformationLocusLine(fileLine);
                } else if (fileLine.startsWith("  ORGANISM")){
                    organismNameCollector(fileLine);
                }
            }
        } catch (java.io.IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Gathers information of the line that contains LOCUS and collects the accession number and the length of the sequence.
     * @param locusLine File line that contains the LOCUS tag.
     */
    private void collectInformationLocusLine(String locusLine){
        String[] locusFragments = locusLine.split(" {4,}");
        accession = locusFragments[1];
        sequenceLength = locusFragments[2];

    }

    /**
     * Count the values in a hashMap that describe the direction of a sequence to calculate the F/R ratio.
     * @param geneLine String of the line that contains the direction of the gene.
     */
    private void forwardReverseHashMapUpdater (String geneLine){
        if (geneLine.contains("complement")) {
            forwardReverseHashMap.put("reverse", forwardReverseHashMap.get("reverse") + 1);
        } else {
            forwardReverseHashMap.put("forward", forwardReverseHashMap.get("forward") + 1);
        }
    }

    /**
     * Counts the amount of gene and CDS tags found in the genBank file.
     * @param tagLabel String of the superTag found.
     */
    private void tagHashMapUpdater(String tagLabel){
        if (tagHashMap.containsKey(tagLabel)) {
            tagHashMap.put(tagLabel, tagHashMap.get(tagLabel) + 1);
        } else {
            tagHashMap.put(tagLabel, 1);
        }
    }

    /**
     * Collects the name of the organism in the file line that contains ORGANISM.
     * @param organismLine String of file line that contains the organism name.
     */
    private void organismNameCollector(String organismLine){
        String[] organismNameLine = organismLine.split(" {2,}");
        // In case something is added behind the organism name.
        String [] pureOrganism = organismNameLine[2].split(" ");
        organism = pureOrganism[0]+" "+pureOrganism[1];


    }

    /**
     * Calculate the forward reverse balance of genes.
     * @return Double that describes the forward reverse balance: F/R.
     */
    private double calculateForwardReverseBalance(){
        double forward = forwardReverseHashMap.get("forward");
        double reverse = forwardReverseHashMap.get("reverse");
        if (reverse == 0){
           return forward;
        } else{
            return Math.round((forward/reverse)*1000)/1000.0d;
        }
    }

    @Override
    public String toString() {

        return "file    " + fileName +
                "\norganism   " + organism +
                "\naccession   " + accession +
                "\nsequence length    " + sequenceLength +
                "\nnumber of genes    " + numberOfGenes +
                "\ngene F/R balance   " + forwardReverseBalance +
                "\nnumber of CDSs     " + numberOfCDS;
    }

}
