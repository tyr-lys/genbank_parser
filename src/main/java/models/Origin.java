package models;

import java.util.ArrayList;

/**
 * Stores the Origin sequence for fetch_gene and fetch_cds.
 */
class Origin {
    private String dNASequence;
    Origin(ArrayList<String> originLines) {
        dNASequence = String.join("",originLines);
    }

    /**
     * Gets a fragment from Origin.
     * @param startIndex Integer to set a start position of a fragment.
     * @param stopIndex Integer to set a stop position of a fragment.
     * @return String fragment of the DNA sequence/origin.
     */
    String originSubstring(int startIndex, int stopIndex){
        return dNASequence.substring(startIndex,stopIndex);
    }
}
