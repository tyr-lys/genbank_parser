package models;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Used for finding features within a specfied range in a genBank File.
 */
public class Features extends SuperTag {
    private ArrayList<Integer> featuresPrintOrderValues;
    private HashMap<Integer,String> featuresDirectionsHashMap;
    private HashMap<Integer,String> featuresFoundNamesHashMap;
    private HashMap<Integer,Integer> featuresStartPositionsHashMap;
    private HashMap<Integer,Integer> featuresStopPositionsHashMap;
    private HashMap<Integer,String> featuresTypeNamesHashMaps;

    /**
     * Finds the features within a genBank File within in a certain range.
     * @param superTagReader Buffered Reader that reads the genBank file.
     * @param chosenInputRange Given range in which the features should be found.
     */
    public Features(BufferedReader superTagReader, String chosenInputRange) {
        super(superTagReader, chosenInputRange);
        featuresPrintOrderValues = new ArrayList<>(super.getSuperTagPrintOrderValues());
        featuresDirectionsHashMap = new HashMap<>(super.getDirectionsHashMap());
        featuresFoundNamesHashMap = new HashMap<>(super.getFoundSuperTagNamesHashMap());
        featuresStartPositionsHashMap = new HashMap<>(super.getStartPositionsHashMap());
        featuresStopPositionsHashMap = new HashMap<>(super.getStopPositionsHashMap());
        featuresTypeNamesHashMaps = new HashMap<>(super.getFoundTypeNamesHashMaps());
    }

    @Override
    public String toString() {
        ArrayList<String> masterString = new ArrayList<>();
        String featuresTemplate = "%s;%s;%s;%s;%s\n";
        masterString.add(">FEATURE;TYPE;START;STOP;ORIENTATION\n");
        for (Integer featureOrderValue: featuresPrintOrderValues){
            masterString.add(String.format(featuresTemplate,featuresFoundNamesHashMap.get(featureOrderValue),
                    featuresTypeNamesHashMaps.get(featureOrderValue),
                    featuresStartPositionsHashMap.get(featureOrderValue)+1,
                    featuresStopPositionsHashMap.get(featureOrderValue),
                    featuresDirectionsHashMap.get(featureOrderValue)));
        }
        return String.join("",masterString);
    }
}
