package models;

import java.io.BufferedReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Used for finding sites in the DNA sequence in the ORIGIN tag.
 */
public class Sites {
    private ArrayList<String> dnaSequenceLines = new ArrayList<>();
    private String wholeDNASequence;
    private Pattern dnaPattern;
    private Integer geneCounter = -1;
    private HashMap<Integer,String> foundNameHashMap = new HashMap<>();
    private HashMap<Integer,Integer> startPositionSuperTagHashMap = new HashMap<>();
    private HashMap<Integer,Integer> stopPositionSuperTagHashMap = new HashMap<>();
    // sequence results
    private HashMap<Integer,String> sequenceHashMap = new HashMap<>();
    private HashMap<Integer,Integer> startPositionSequenceHashMap = new HashMap<>();
    private HashMap<Integer,Integer> stopPositionSequenceHashMap = new HashMap<>();
    private String result;

    /**
     * Finds sites composed out of a specified DNA sequence.
     * @param siteReader Buffered reader that reads a genBank file.
     * @param dnaSearchPattern String which represents a DNA pattern according to the IUPAC standard.
     */
    public Sites(BufferedReader siteReader, String dnaSearchPattern) {
        siteFinder(siteReader);
        dnaSearchPattern = searchPatternToIUPAC(dnaSearchPattern);
        dnaPattern = Pattern.compile(dnaSearchPattern);
        searchPatternsInDNASequence();
        result = applyRightCombinations();

    }

    /**
     * Reads the genBank file lines and determines what to do with certain file lines.
     * @param siteReader Buffered reader for reading genBank files.
     */
    private void siteFinder(BufferedReader siteReader){
        String fileLine;
        boolean endOfGenbank = false;
        boolean passedCommentString = false;
        boolean withinSuperTag = false;
        Pattern geneCDSPattern = Pattern.compile("(^ {5,20}gene)|(^ {5,20}\\w+)");
        try {
            while ((fileLine = siteReader.readLine()) != null) {
                Matcher geneCDSMatcher = geneCDSPattern.matcher(fileLine);
                if (endOfGenbank) {
                    if (!fileLine.startsWith("//"))
                        dnaSequenceLines.add(fileLine.replaceAll("[\n 0-9]+",""));
                    else{
                        wholeDNASequence = String.join("",dnaSequenceLines);
                        break;
                    }
                } else if (fileLine.startsWith("ORIGIN")) {
                    endOfGenbank = true;
                } else if (withinSuperTag && geneCDSMatcher.find()){
                    if (geneCDSMatcher.group(0).contains("gene")) {
                        geneCounter += 1;
                        findStartAndStopPositions(fileLine);
                        findNames(fileLine);
                    } else{
                        withinSuperTag = false;
                    }
                } else if (withinSuperTag){
                    findNames(fileLine);
                } else if (passedCommentString){
                    if (geneCDSMatcher.find()){
                        if (geneCDSMatcher.group(0).contains("gene")) {
                            geneCounter += 1;
                            withinSuperTag = true;
                            findStartAndStopPositions(fileLine);
                        }
                    }
                } else if (fileLine.startsWith("FEATURES")) {
                    passedCommentString = true;
                }

            }
        }catch (java.io.IOException e) {
                // Should not happen.
            }


    }

    /**
     * Find start and stop position within a fileline.
     * @param fileLine String that represents a file line.
     */
    private void findStartAndStopPositions(String fileLine){
        String[] superTagInformationFragments = fileLine.split(" {13,}");
        String [] positionSuperTagFragment;
        if (superTagInformationFragments.length > 1){
            positionSuperTagFragment = superTagInformationFragments;
        } else {
            positionSuperTagFragment = superTagInformationFragments[0].split(" {12,}");
        }
        positionFragmentCollection(positionSuperTagFragment[1].replaceAll("[<>]",""));
    }

    /**
     * Filter start and stop position from a superTag and store it.
     * @param fragmentsWithPosition String fragment from fileLine that contains start and stop Position.
     */
    private void positionFragmentCollection (String fragmentsWithPosition){
        String [] startStopPosition;
        String startPosition;
        String stopPosition;
        startStopPosition = fragmentsWithPosition.split("\\.\\.");
        startPosition = startStopPosition[0];
        stopPosition = startStopPosition[1];
        if (startPosition.contains("complement(")){
            String [] redoneStartposition = startPosition.split("\\(");
            startPosition = redoneStartposition[1];
        }
        if (stopPosition.endsWith(")")){
            stopPosition = stopPosition.substring(0,stopPosition.length()-1);

        }
        startPositionSuperTagHashMap.put(geneCounter,Integer.parseInt(startPosition));
        stopPositionSuperTagHashMap.put(geneCounter,Integer.parseInt(stopPosition));
    }

    /**
     * Find name of a gene within a subTag
     * @param fileLine String that could contain information regarding the name of the gene.
     */
    private void findNames(String fileLine){
        String [] fileLineWithSubTag;
        if (fileLine.contains("/gene")){
            fileLineWithSubTag = fileLine.split("=");
            foundNameHashMap.put(geneCounter,fileLineWithSubTag[1].replace("\"",""));
        } else if (fileLine.contains("/locus_tag")){
            fileLineWithSubTag = fileLine.split("=");
            foundNameHashMap.put(geneCounter,fileLineWithSubTag[1].replace("\"",""));
        }

    }

    /**
     * Find the regex pattern in the DNA that was specified.
     */
    private void searchPatternsInDNASequence(){
        Matcher dnaPatterenMatcher = dnaPattern.matcher(wholeDNASequence);
        Integer foundSequenceMatches = -1;
        while (dnaPatterenMatcher.find()){
            foundSequenceMatches += 1;
            sequenceHashMap.put(foundSequenceMatches,dnaPatterenMatcher.group());
            startPositionSequenceHashMap.put(foundSequenceMatches,dnaPatterenMatcher.start()+1);
            stopPositionSequenceHashMap.put(foundSequenceMatches,dnaPatterenMatcher.end()+1);

        }
    }

    /**
     * Makes a regex out of valid IUPAC characters, if not valid an n character will be added.
     * @param dnaPattern Regex DNA IUPAC pattern.
     * @return String that is a regex pattern.
     */
    private String searchPatternToIUPAC(String dnaPattern){
        StringBuilder convertedIUPACCode = new StringBuilder();
        convertedIUPACCode.append("(");
        for(char nucleotide: dnaPattern.toLowerCase().toCharArray()){
            switch(nucleotide){
                case 'a' :
                    convertedIUPACCode.append(nucleotide);
                    break;
                case 'c' :
                    convertedIUPACCode.append(nucleotide);
                    break;
                case 't':
                    convertedIUPACCode.append(nucleotide);
                    break;
                case 'g':
                    convertedIUPACCode.append(nucleotide);
                    break;
                case 'r':
                    convertedIUPACCode.append("[ag]");
                    break;
                case 'y':
                    convertedIUPACCode.append("[ct]");
                    break;
                case 's':
                    convertedIUPACCode.append("[gc]");
                    break;
                case 'w':
                    convertedIUPACCode.append("[at]");
                    break;
                case 'k':
                    convertedIUPACCode.append("[gt]");
                    break;
                case 'm':
                    convertedIUPACCode.append("[ac]");
                    break;
                case 'b':
                    convertedIUPACCode.append("[cgt]");
                    break;
                case 'd':
                    convertedIUPACCode.append("[agt]");
                    break;
                case 'h':
                    convertedIUPACCode.append("[act]");
                    break;
                case 'v':
                    convertedIUPACCode.append("[acg]");
                    break;
                case 'n':
                    convertedIUPACCode.append("[acgt]");
                    break;
                default:
                    System.out.println(String.format("%s is not a valid nucleotide, no character will be added.",nucleotide));
                    break;
                }
            }
            convertedIUPACCode.append(")");
            return convertedIUPACCode.toString();
        }

    /**
     * Tests if the options are within the specfied range, if not it will be called intergenic.
     * @return A string of the correct pattern combinations, each gene is separated by a newline character.
     */
    private String applyRightCombinations(){
        StringBuilder allFoundGenes = new StringBuilder();
        for(int i =0; i < startPositionSequenceHashMap.size(); i++) {
            for (int j = 0; j < startPositionSuperTagHashMap.size(); j++) {
                if (startPositionSequenceHashMap.get(i) >= startPositionSuperTagHashMap.get(j) &&
                stopPositionSequenceHashMap.get(i) <= stopPositionSuperTagHashMap.get(j)){
                    allFoundGenes.append(String.format("%s;%s;%s\n",startPositionSequenceHashMap.get(i),sequenceHashMap.get(i).toUpperCase(),foundNameHashMap.get(j)));
                    break;
                } else if( j == startPositionSuperTagHashMap.size()-1){
                    allFoundGenes.append(String.format("%s;%s;%s\n",startPositionSequenceHashMap.get(i),sequenceHashMap.get(i).toUpperCase(),"INTERGENIC"));
                }
            }
        }
        return allFoundGenes.toString();
    }

    @Override
    public String toString() {
        return String.format("POSITION;SEQUENCE;GENE\n%s",result);
    }
}




