package models;

import java.io.BufferedReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * Defines the base for gene and CDS and also makes fetch_features possible.
 */
class SuperTag {
    // fetch_gene/CDS
    private HashMap<Integer, Integer> startPositionsHashMap = new HashMap<>();
    private HashMap<Integer, Integer> stopPositionsHashMap = new HashMap<>();
    private HashMap<Integer, String> directionsHashMap = new HashMap<>();
    private HashMap<Integer, String> foundSuperTagNamesHashMap = new HashMap<>();
    private ArrayList<String> originLines = new ArrayList<>();
    Origin superTagOrigin;

    // fetch_features
    private HashMap<Integer, String> foundTypeNamesHashMaps = new HashMap<>();
    private Integer startRange;
    private Integer stopRange;
    private Boolean featuresFilter;
    private Boolean featureInRange = false;
    private Integer sequenceLength;

    private ArrayList<Integer> superTagPrintOrderValues;
    private ArrayList<String> temporaryStorage = new ArrayList<>();
    private Pattern superTagPattern;
    private Pattern locusTagPattern;
    private Pattern requestedSuperTagPattern = Pattern.compile("^ {5,}\\w+ {6,}");
    private int foundSuperTagCounter = 0;
    private String superTag;
    private String stringValueInput;

    /** Constructor for fetch_gene and fetch_cds
     * @param superTagReader The BufferedReader which reads the genBankFile.
     * @param targetSuperTagName The String/regexPattern that describes a named that is looked for.
     * @param tagDeterminingString The String that is used for searching the specific label.
     * @param requestedSuperTag The String which determines if there should be looked for gene or CDS.
     */
    SuperTag(BufferedReader superTagReader, String targetSuperTagName, String tagDeterminingString, String requestedSuperTag) {

        String superTagRegex = "/%s=\"%s\"";
        String locusTagRegex = "/locus_tag=\"%s\"";
        superTag = requestedSuperTag;
        featuresFilter = false;

        superTagPattern = Pattern.compile(String.format(superTagRegex, tagDeterminingString, targetSuperTagName));
        locusTagPattern = Pattern.compile(String.format(locusTagRegex, targetSuperTagName));

        superTagFilter(superTagReader);
        superTagPrintOrder();
    }

    /**
     * Constructor for fetch_features.
     * @param superTagReader Buffered reader used for reading the genBankFile.
     * @param chosenInputRange The range in which should be looked for genes.
     */
    SuperTag(BufferedReader superTagReader, String chosenInputRange) {
        featuresFilter = true;
        //todo change order with feature filter
        stringValueInput = chosenInputRange;
        featureFilter(superTagReader);
        superTagPrintOrder();
    }


    /**
     * Reads the genBankFile and filters the information that is required for getting the features within a certain range.
     * @param fileReader BufferedReader for file reading.
     */
    private void featureFilter(BufferedReader fileReader) {
        Boolean passedTheCommentString = false;
        String fileLine;
        Pattern geneCDSPattern = Pattern.compile("(^ {5,20}gene)|(^ {5}\\w+)");

        try {
            while ((fileLine = fileReader.readLine()) != null) {
                if (fileLine.contains("LOCUS")){
                    String [] locusFragments = fileLine.split(" {4,}");
                    sequenceLength = Integer.parseInt(locusFragments[2].replaceAll("\\D+",""));
                    givenInputValueProcessor(stringValueInput);
                }
                if (passedTheCommentString) {
                    Matcher geneCDSMatcher = geneCDSPattern.matcher(fileLine);
                    // The end, origin is not needed.
                    if (fileLine.startsWith("ORIGIN")) {
                        break;
                    }
                    if (featureInRange && geneCDSMatcher.find()) {
                        lastChanceForNameFromSubTag();
                        superTagPositionDirectionSetter(fileLine);

                    } else if (featureInRange) {
                        String subTagResult = findSubTag(fileLine);
                        if (subTagResult != null) {
                            foundSuperTagNamesHashMap.put(foundSuperTagCounter, subTagResult
                                    //todo watch this for input sake!
                                    .replace("\"", ""));
                            featureInRange = false;
                            temporaryStorage.clear();
                        } else {
                            temporaryStorage.add(fileLine);
                        }
                    }
                    if (geneCDSMatcher.find()) {
                        if (geneCDSMatcher.group(0).contains("gene") || geneCDSMatcher.group(0).contains("CDS")) {
                            superTagPositionDirectionSetter(fileLine);
                        }
                    }
                    // FIRST STEPS
                } else {
                    if (fileLine.contains("FEATURES")) {
                        passedTheCommentString = true;
                    }
                }
            }
        } catch (java.io.IOException e) {
            // Should not happen.
        }

    }

    /** Test whether a certain feature "lives" within in the specified range.
     * @param startStopValueArray Array of Strings that has the start and stop values.
     * @return Boolean that states if the found feature is found within in the range.
     */
    private boolean checkInRange(String[] startStopValueArray) {
        if (Integer.parseInt(startStopValueArray[0]) >= startRange && Integer.parseInt(startStopValueArray[1]) <= stopRange) {
            foundSuperTagCounter += 1;
            featureInRange = true;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Catches the information if no /gene tag is found within the collected information, a locus tag will be used.
     * If none is found it will be called nameless.
     */
    private void lastChanceForNameFromSubTag() {
        boolean named = false;
        for (String collectedLine : temporaryStorage) {
            if (collectedLine.contains("/gene")) {
                String[] lineWithName = collectedLine.split("=");
                foundSuperTagNamesHashMap.put(foundSuperTagCounter, lineWithName[1].replace("\"", ""));
                named = true;
                break;
            } else if (collectedLine.contains("/locus_tag")) {
                String[] lineWithName = collectedLine.split("=");
                foundSuperTagNamesHashMap.put(foundSuperTagCounter, lineWithName[1].replace("\"", ""));
                named = true;
                break;
            }
        }
        temporaryStorage.clear();
        if (!named) {
            foundSuperTagNamesHashMap.put(foundSuperTagCounter, "Nameless");
        }
    }

    /** Gives the name back of the product.
     * @param fileLine String which is a line of the genBankFile that describes the feature.
     * @return Gives the productName back.
     */
    private String findSubTag(String fileLine) {
        String[] informativeNameLine;
        if (fileLine.contains("/product")) {
            informativeNameLine = fileLine.split("=");
            return informativeNameLine[1];
        }
        return null;
    }

    /**
     * Reads the genBankFile, collects and processes the information.
     * @param fileReader BufferedReader which reads the genBankFile.
     */
    private void superTagFilter(BufferedReader fileReader) {
        String fileLine;
        boolean requestedSuperTagFound = false;
        boolean endOfGenbank = false;
        boolean passedTheCommentString = false;
        try {
            while ((fileLine = fileReader.readLine()) != null) {
                if (passedTheCommentString) {
                    Matcher requestedSuperTagMatcher = requestedSuperTagPattern.matcher(fileLine);
                    // Final steps
                    if (endOfGenbank) {
                        endOfGenBankProcessor(fileLine);
                    }
                    if (requestedSuperTagFound && requestedSuperTagMatcher.find()) {
                        if (requestedSuperTagMatcher.group().contains(superTag)) {
                            // Nothing found, new gene tag.
                            temporaryStorage.clear();
                            temporaryStorage.add(fileLine);
                        } else {
                            // Nothing found, no gene tag.
                            requestedSuperTagFound = false;
                            temporaryStorage.clear();
                        }
                    } else if (requestedSuperTagFound) {
                        requestedSuperTagFound = subTagDetector(fileLine);
                    }
                    // First steps
                    if (requestedSuperTagMatcher.find()) {

                        if (requestedSuperTagMatcher.group().contains(superTag)) {
                            temporaryStorage.add(fileLine);
                            requestedSuperTagFound = true;
                        }
                    } else if (fileLine.startsWith("ORIGIN")) {
                        endOfGenbank = true;
                    }
                } else {
                    if (fileLine.contains("FEATURES")) {
                        passedTheCommentString = true;
                    }
                }
            }
        } catch (java.io.IOException e) {
            // Should not happen.
        }
    }

    /**
     * If ORIGIN is found within the line is should collect all lines and remove the digits,
     * spaces and newline characters to form a DNA sequence.
     * @param fileLine String of the given fileLine.
     */
    private void endOfGenBankProcessor(String fileLine) {
        if (!fileLine.startsWith("//")) {
            originLines.add(fileLine.replaceAll(" +|\\d+|\\n", ""));
        } else {
            superTagOrigin = new Origin(originLines);
        }
    }

    /**
     * Detects whether a subTag is found which suits and if a superTag is found which determines if
     * the information should be dumped or should be stored.
     * @param fileLine String of the fileLine which is processed.
     * @return Boolean which is a result if there was a subTagSequenceFound.
     */
    private boolean subTagDetector(String fileLine) {
        String foundSuperTagSequence = findSuperTagNameWithSubTags(fileLine, superTagPattern, locusTagPattern);
        if (foundSuperTagSequence != null) {
            foundSuperTagCounter += 1;
            foundSuperTagNamesHashMap.put(foundSuperTagCounter, foundSuperTagSequence);
            superTagPositionDirectionSetter(temporaryStorage.get(0));
            temporaryStorage.clear();
            return false;
        } else {
            return true;
        }
    }

    /**
     * Find subtag that describes the name of the found superTag.
     * @param fileLine String of the fileLine which might contain a subTag.
     * @param superTagPattern The pattern that defines if the name of the subTag which should be looked for.
     * @param locusTagPattern The pattern that defines what name should be looked for in the locus_tag.
     * @return String that contains processed information ready to be inserted into the hashMap for names.
     */
    private String findSuperTagNameWithSubTags(String fileLine, Pattern superTagPattern, Pattern locusTagPattern) {
        Matcher superTagSubTagMatcher = superTagPattern.matcher(fileLine);
        if (superTagSubTagMatcher.find()) {
            if (superTag.equals("gene")) {
                return superTagSubTagMatcher.group().substring(7, superTagSubTagMatcher.group().length() - 1);
            } else if (superTag.equals("CDS")) {
                return superTagSubTagMatcher.group().substring(10, superTagSubTagMatcher.group().length() - 1);
            }
        } else {
            Matcher locusSubTagMatcher = locusTagPattern.matcher(fileLine);
            if (locusSubTagMatcher.find()) {
                return locusSubTagMatcher.group().substring(12, locusSubTagMatcher.group().length() - 1);
            }
        }
        return null;
    }

    /** Gets the start and stop position of a line which contains the superTag.
     * @param detectedSuperTagLine String of the line which contains the superTag.
     */
    private void superTagPositionDirectionSetter(String detectedSuperTagLine) {
        String[] positionSuperTagFragment;
        String[] superTagInformationFragments = detectedSuperTagLine.split(" {13,}");
        if (superTagInformationFragments.length > 1) {
            positionSuperTagFragment = superTagInformationFragments;
        } else {
            positionSuperTagFragment = superTagInformationFragments[0].split(" {12,}");
        }
        if (positionSuperTagFragment.length == 1){
            positionSuperTagFragment = positionSuperTagFragment[0].split(" {11,}");
        }
        addDirectionToDirectionHashMap(positionSuperTagFragment);
    }

    /** Determines if the superTag requires a complement or a normal direction.
     * @param positionSuperTagFragment String Array of the start and stop positions.
     */
    private void addDirectionToDirectionHashMap(String[] positionSuperTagFragment) {
        if (positionSuperTagFragment[1].contains("complement(")) {
            String[] startStopPositions = reverseFragmentCheck(positionSuperTagFragment[1]);
            fetchDirectionChoice(startStopPositions, "R", positionSuperTagFragment[0].trim());

        } else {
            String[] startStopPositions = forwardFragmentCheck(positionSuperTagFragment[1]);
            fetchDirectionChoice(startStopPositions, "F", positionSuperTagFragment[0].trim());
        }
    }

    /**
     * Determines what should be done with the information depending on if fetch_gene/fetch_cds is used or fetch_features.
     * @param startStopPositions String [] that contains the start and stop position of a superTag.
     * @param direction String that describes the direction of the superTag.
     * @param superTagName Found name of superTag only for fetch_gene and fetch_cds.
     */
    private void fetchDirectionChoice(String[] startStopPositions, String direction, String superTagName) {
        if (featuresFilter && checkInRange(startStopPositions)) {
            foundTypeNamesHashMaps.put(foundSuperTagCounter, superTagName);
            startStopDirectionHashMapUpdater(startStopPositions, direction);
        } else if (!featuresFilter) {
            startStopDirectionHashMapUpdater(startStopPositions, direction);
        }
    }

    /**
     * Stores the superTag information in the hashMaps.
     * @param startStopPositionsOfFragment String array of the start and stop positions.
     * @param direction The direction of the superTag.
     */
    private void startStopDirectionHashMapUpdater(String[] startStopPositionsOfFragment, String direction) {
        startPositionsHashMap.put(foundSuperTagCounter, Integer.parseInt(startStopPositionsOfFragment[0]) - 1);
        stopPositionsHashMap.put(foundSuperTagCounter, Integer.parseInt(startStopPositionsOfFragment[1]));
        directionsHashMap.put(foundSuperTagCounter, direction);
    }

    /**
     * Removes al the none describing information about a superTags start and stop position if it is a reverse complement.
     * @param numberFragment String that contains start and stop positions.
     * @return String array of the start and stop positions.
     */
    private String[] reverseFragmentCheck(String numberFragment) {
        numberFragment = numberFragment.replaceAll("[<>]", "");
        return numberFragment.substring(11,
                numberFragment.length() - 1).split("\\.\\.");
    }

    /**
     * Removes al the none describing information about a superTags start and stop position if it comes from a forward strand.
     * @param numberFragment String that contains start and stop positions.
     * @return String array of the start and stop positions.
     */
    private String[] forwardFragmentCheck(String numberFragment) {
        numberFragment = numberFragment.replaceAll("[<>]", "");
        return numberFragment.split("\\.\\.");
    }


    /**
     * Reforms a DNA sequence that results in sequences of 80 characters or less if it is short piece remaining.
     * @param superTagDNASequence String of DNA sequence that needs te be reshaped in strings of 80 characters.
     * @return String with newline characters that contains a DNA sequence that has 80 characters.
     */
    StringBuilder sequenceLengthManager(String superTagDNASequence) {
        StringBuilder managedSuperTagDNASequence = new StringBuilder();
        for (int i = 0; i < superTagDNASequence.length(); i += 80) {
            try {
                managedSuperTagDNASequence.append(superTagDNASequence.substring(i, i + 80));
                managedSuperTagDNASequence.append("\n");
            } catch (StringIndexOutOfBoundsException e) {
                managedSuperTagDNASequence.append(superTagDNASequence.substring(i));
            }
        }
        return managedSuperTagDNASequence;
    }

    /**
     * Determines in which order the information has to be printed, based on the starting value of sequences.
     */
    private void superTagPrintOrder() {
        if (startPositionsHashMap.size() == 1) {
            superTagPrintOrderValues = new ArrayList<>(startPositionsHashMap.keySet());
        } else {
            /*
             * https://stackoverflow.com/questions/780541/how-to-sort-a-hashmap-in-java
             * 2014 Vitalii Fedorenko.
             */
            Comparator<Map.Entry<Integer, Integer>> valueComparator =
                    Comparator.comparingInt(Map.Entry::getValue);
            Map<Integer, Integer> sortedMap = startPositionsHashMap.entrySet().stream().sorted(valueComparator).collect(Collectors.toMap(
                    Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
            superTagPrintOrderValues = new ArrayList<>(sortedMap.keySet());
        }
    }

    /**
     * Sets the start and stop position of in which ranges at features should be looked,
     * if longer than the sequence the maximum value will be used if shorter than the sequence 1 will be used.
     * @param inputRangeValues Start and stop values of the given input string.
     */
    private void givenInputValueProcessor(String inputRangeValues) {
        String[] stringRangeValues = inputRangeValues.split("\\.\\.");
        if (Integer.parseInt(stringRangeValues[0]) < 1){
            startRange = 1;
        } else {
            startRange = Integer.parseInt(stringRangeValues[0]);
        }
        if (Integer.parseInt(stringRangeValues[1]) > sequenceLength) {
            stopRange = sequenceLength;
        } else {
            stopRange = Integer.parseInt(stringRangeValues[1]);
        }
    }


    /**
     * @return Gets all the start positions of the superTags.
     */
    HashMap<Integer, Integer> getStartPositionsHashMap() {
        return startPositionsHashMap;
    }

    /**
     * @return Gets all the stop positions of the superTags.
     */
    HashMap<Integer, Integer> getStopPositionsHashMap() {
        return stopPositionsHashMap;
    }

    /**
     * @return Gets all the directions of the superTags.
     */
    HashMap<Integer, String> getDirectionsHashMap() {
        return directionsHashMap;
    }

    /**
     * @return Gets all the names of the superTags.
     */
    HashMap<Integer, String> getFoundSuperTagNamesHashMap() {
        return foundSuperTagNamesHashMap;
    }

    /**
     * @return Gets the order of which the results should be printed.
     */
    ArrayList<Integer> getSuperTagPrintOrderValues() {
        return superTagPrintOrderValues;
    }

    /**
     * @return Gets all the types of the superTags which should be printed.
     */
    HashMap<Integer, String> getFoundTypeNamesHashMaps() {
        return foundTypeNamesHashMaps;
    }
}
