package models;

import dnasequenceutilities.DnaUtilities;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Finds gene superTags in genBank file.
 */
public class Genes extends SuperTag {
    private ArrayList<Integer> genePrintOrderValues;
    private HashMap<Integer,String> geneDirectionsHashMap;
    private HashMap<Integer,String> geneFoundNamesHashMap;
    private HashMap<Integer,Integer> geneStartPositionsHashMap;
    private HashMap<Integer,Integer> geneStopPositionsHashMap;


    /**
     * Inherits from SuperTag and gets all the values it needs to form a Genes object.
     * @param superTagReader Buffered Reader that reads the genBank.
     * @param targetSuperTagName String of the gene that is looked for.
     * @param tagDeterminingString String of the subTag that should be used to look for name of the gene.
     * @param requestedSuperTag String of the superTag that is looked for.
     */
    public Genes(BufferedReader superTagReader, String targetSuperTagName, String tagDeterminingString, String requestedSuperTag) {
        super(superTagReader, targetSuperTagName, tagDeterminingString, requestedSuperTag);
        genePrintOrderValues = new ArrayList<>(super.getSuperTagPrintOrderValues());
        geneDirectionsHashMap = new HashMap<>(super.getDirectionsHashMap());
        geneFoundNamesHashMap = new HashMap<>(super.getFoundSuperTagNamesHashMap());
        geneStartPositionsHashMap = new HashMap<>(super.getStartPositionsHashMap());
        geneStopPositionsHashMap = new HashMap<>(super.getStopPositionsHashMap());

    }
    @Override
    public String toString() {
        ArrayList<String> masterString = new ArrayList<>();
        String geneSequence;
        String stringResultTemplate = ">gene %s Sequence\n%s\n\n";
        for (Integer geneOrderValue: genePrintOrderValues){
            if(geneDirectionsHashMap.get(geneOrderValue).equals("R")){
                geneSequence = DnaUtilities.dnaComplementMaker(superTagOrigin.originSubstring(geneStartPositionsHashMap.get(geneOrderValue),geneStopPositionsHashMap.get(geneOrderValue)));
            } else{
                geneSequence = superTagOrigin.originSubstring(geneStartPositionsHashMap.get(geneOrderValue),geneStopPositionsHashMap.get(geneOrderValue));
            }
            masterString.add(String.format(stringResultTemplate, geneFoundNamesHashMap.get(geneOrderValue),sequenceLengthManager(geneSequence)));
        }
        return String.join("",masterString);
    }
}
