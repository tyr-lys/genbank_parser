package dnasequenceutilities;

/**
 * A collection of simple utilities that modify DNA sequences
 */
public class DnaUtilities {

    /**
     * Makes a complement of the submitted DNA sequence.
     * @param dnaSequence String of the DNA sequence.
     * @return String that is the reverse complement of the original DNA sequence.
     */
    public static String dnaComplementMaker(String dnaSequence){
        StringBuilder reverseComplement = new StringBuilder();
        for (char nucleotide: dnaSequence.toLowerCase().toCharArray()){
            switch(nucleotide){
                case 'a' :
                    reverseComplement.append("t");
                    break;
                case 'c' :
                    reverseComplement.append("g");
                    break;
                case 'g' :
                    reverseComplement.append("c");
                    break;
                case 't' :
                    reverseComplement.append("a");
                    break;
                default:
                    reverseComplement.append("n");
            }
        }
        return reverseComplement.reverse().toString();
    }

    /**
     * DNA sequence will be converted to an Amino Acid sequence, it is done triplet by triplet.
     * @param dnaSequence String that represents a DNA sequence.
     * @return Gives back an amino acid sequence
     */
    public static String convertNucleotidesToAminoAcids(String dnaSequence){
        StringBuilder aminoAcidSequence = new StringBuilder();
        for(int i = 0; i < dnaSequence.length(); i+=3){
            try {
                aminoAcidSequence.append(translateDna(dnaSequence.substring(i, i + 3)));
            } catch (StringIndexOutOfBoundsException e){
                // Just ignore it.
            }
        }
        return aminoAcidSequence.toString();
    }


    /**
     * Converts DNA triplets to their corresponding amino acids.
     * @param codon A triplet of nucleotides.
     * @return Gives back the amino acid that corresponds with the DNA triplet.
     */
    private static String translateDna(String codon){
        String aminoAcid;
        switch(codon){
            case "TTT":case "TTC":
                aminoAcid="F";
                break;
            case "TTA":case "TTG":case "CTT":case"CTC":case"CTA":case "CTG":
                aminoAcid="L";
                break;
            case "ATT":case "ATC":case "ATA":
                aminoAcid="I";
                break;
            case "ATG":
                aminoAcid="M";
                break;
            case "GTT":case "GTC":case "GTA":case "GTG":
                aminoAcid="V";
                break;
            case "TCT":case "TCC":case "TCA":case "TCG":
                aminoAcid="S";
                break;
            case "CCA":case "CCC":case "CCG":case "CCT":
                aminoAcid="P";
                break;
            case "ACT":case "ACC":case "ACA":case "ACG":
                aminoAcid="T";
                break;
            case "GCT":case "GCC":case "GCA":case "GCG":
                aminoAcid="A";
                break;
            case "TAT":case "TAC":
                aminoAcid="Y";
                break;
            case "TAA":case "TAG":case "TGA":
                aminoAcid="*";
                break;
            case "CAT":case "CAC":
                aminoAcid="H";
                break;
            case "CAA":case "CAG":
                aminoAcid="Q";
                break;
            case "AAT":case "AAC":
                aminoAcid="N";
                break;
            case "AAA":case "AAG":
                aminoAcid="K";
                break;
            case "GAT":case "GAC":
                aminoAcid="D";
                break;
            case "GAA":case "GAG":
                aminoAcid="E";
                break;
            case "TGT":case "TGC":
                aminoAcid="C";
                break;
            case "TGG":
                aminoAcid="W";
                break;
            case "CGT":case "CGC":case "CGA":case "CGG": case "AGA":case "AGG":
                aminoAcid="R";
                break;
            case "AGT":case "AGC":
                aminoAcid="S";
                break;
            case "GGT":case "GGC":case "GGA":case "GGG":
                aminoAcid="G";
                break;
            default:
                throw new IllegalArgumentException("Invalid nucleotides where used.");
        }
        return aminoAcid;
    }
}

