package cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Option;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Contains all classes and tests that make it possible to run the commandline parser.
 */
public class GenBankReaderCliOptions {
    private static Options allOptions;
    private static LinkedList<String> longOptionLinkedList = new LinkedList<>(Arrays.asList("fetch_gene",
            "fetch_cds", "fetch_features", "find_sites"));
    private String selectedOption;
    private String infileOption;
    private String selectedOptionInputValue;
    private boolean inputIsCorrect;

    /**
     * Makes all the options that are available to this tool and fills in the correct
     * attributes with the correct arguments.
     * @param args The commandline arguments.
     */
    public GenBankReaderCliOptions(String[] args) {
        allOptions = createHelpOrRunOptions();
        chooseHelpOrRun(allOptions, args);
    }

    /**
     * Tests whether the summary option is filled in correctly, if not the program will shutdown.
     * @param cmd The collected commandline arguments.
     */
    private void summaryOptionTest(CommandLine cmd) {
        if (invalidExtraArgumentTest(cmd)) {
            inputIsCorrect = false;
            callHelpOnError(allOptions);
        } else {
            inputIsCorrect = true;
            selectedOption = "summary";
        }
    }

    /** Tests if there are too many arguments filled in when fetch_gene is used.
     * @param cmd The collected commandline arguments.
     */
    private void fetchGeneOptionTest(CommandLine cmd) {
        if (invalidExtraArgumentTest(cmd)) {
            inputIsCorrect = false;
            callHelpOnError(allOptions);
        } else {
            inputIsCorrect = true;
            selectedOption = "fetch_gene";
            selectedOptionInputValue = cmd.getOptionValue("fetch_gene");
        }
    }

    /** Tests if there are too many arguments filled in when fetch_cds is used.
     * @param cmd The collected commandline arguments.
     */
    private void fetchCDSOptionTest(CommandLine cmd) {
        if (invalidExtraArgumentTest(cmd)) {
            callHelpOnError(allOptions);
            inputIsCorrect = false;
        } else {
            inputIsCorrect = true;
            selectedOption = "fetch_cds";
            selectedOptionInputValue = cmd.getOptionValue("fetch_cds");
        }
    }

    /** Tests: if the arguments are integers, there are too many options filled in,
     * tests if the start number is larger than the stop number.
     * @param cmd The collected commandline arguments.
     */
    private void fetchFeaturesOptionTest(CommandLine cmd) {
        if (invalidExtraArgumentTest(cmd)) {
            inputIsCorrect = false;
        }
        String commandLineInput = cmd.getOptionValue("fetch_features");
        String[] splitInput = commandLineInput.split("\\.\\.");
        try {
            if (splitInput.length > 2 || splitInput.length == 1) {
                inputIsCorrect = false;
                callHelpOnError(allOptions);
            } else if (Integer.parseInt(splitInput[0]) < Integer.parseInt(splitInput[1])) {
                inputIsCorrect = true;
                selectedOption = "fetch_features";
                selectedOptionInputValue = cmd.getOptionValue("fetch_features");
            }
        } catch (NumberFormatException e) {
            inputIsCorrect = false;
            callHelpOnError(allOptions);
        }
    }

    /**Tests if there are too many arguments and if none IUPAC characters are available in the given input.
     * @param cmd The collected commandline arguments.
     */
    private void findSitesOptionTest(CommandLine cmd) {
        Pattern notIUPACPattern = Pattern.compile("[^ACGTRYSWKMBDHVNacgtryswkmbdhvn]");

        if (invalidExtraArgumentTest(cmd)) {
            inputIsCorrect = false;
            callHelpOnError(allOptions);
        }
        Matcher notIUPACMatcher = notIUPACPattern.matcher(cmd.getOptionValue("find_sites"));
        if(notIUPACMatcher.find()){
            inputIsCorrect = false;
            callHelpOnError(allOptions);
        }else{
            inputIsCorrect = true;
            selectedOption = "find_sites";
            selectedOptionInputValue = cmd.getOptionValue("find_sites");
        }

    }

    /** Validates if the user has given the option --infile or --help, with --help the help function is called.
     * With --infile extra options have to be specified.
     * @param helpRunArguments All made options.
     * @param args Commandline arguments.
     */
    private void chooseHelpOrRun(Options helpRunArguments, String[] args) {
        CommandLineParser helpOrRunParser = new DefaultParser();
        try {
            CommandLine cmd = helpOrRunParser.parse(helpRunArguments, args);
            if (cmd.hasOption("infile")) {
                infileOption = cmd.getOptionValue("infile");

                correctArgumentTester(cmd);
            }

        } catch (ParseException e) {
            // Catches: Missing arguments from --infile and directly calls help.
            callHelpOnError(allOptions);
            inputIsCorrect = false;
        }
    }

    /**  Calls the help from the program.
     * @param helpRunArguments All specified help options.
     */
    private static void callHelpOnError(Options helpRunArguments) {
        HelpFormatter genBankHelpFormatter = new HelpFormatter();
        genBankHelpFormatter.setOptionComparator(null);
        genBankHelpFormatter.printHelp("GenBankReader", helpRunArguments);
    }

    /** Tests which commandline option is specified on the commandline and calls for the test associated with the option.
     * @param cmd Commandline options;
     */
    private void correctArgumentTester(CommandLine cmd) {
        if (cmd.hasOption("summary")) {
            summaryOptionTest(cmd);
        } else if (cmd.hasOption("fetch_gene")) {
            fetchGeneOptionTest(cmd);
        } else if (cmd.hasOption("fetch_cds")) {
            fetchCDSOptionTest(cmd);
        } else if (cmd.hasOption("fetch_features")) {
            fetchFeaturesOptionTest(cmd);
        } else if (cmd.hasOption("find_sites")) {
            findSitesOptionTest(cmd);
        } else {
            inputIsCorrect = false;
        }
    }

    /** Tests if there are extra arguments in specified besides the positional/required arguments.
     * @param cmd Commandline options.
     * @return Boolean depending on the unspecified options.
     */
    private static boolean invalidExtraArgumentTest(CommandLine cmd) {
        boolean invalidAmountOfArguments = false;
        if (cmd.getArgList().size() > 0) {
            invalidAmountOfArguments = true;
        }
        return invalidAmountOfArguments;
    }

    /** Makes the help and infile options which the program can execute including descriptive strings,
     * separate method for the --infile options.
     * @return All made options for the program.
     */
    private static Options createHelpOrRunOptions() {
        Options helpAndRunOptions = new Options();
        OptionGroup helpRunGroupOptions = new OptionGroup();
        helpRunGroupOptions.addOption(
                Option.builder(null)
                        .longOpt("help")
                        .hasArg(false)
                        .desc("Prints this message and the available options for " +
                                "the program, this option overrules all other options.")
                        .build()
        );
        helpRunGroupOptions.addOption(
                Option.builder(null)
                        .hasArg(true)
                        .numberOfArgs(1)
                        .longOpt("infile")
                        .desc("Fill in a Genbank file with full path if necessary AND ONE! of its options. " +
                                "Read the options in the help for more information and their arguments." +
                                "\n\"--summary\"" +
                                "\n \"--fetch_gene\"" +
                                "\n \"--fetch_cds\"" +
                                "\n \"--fetch_features\"" +
                                "\n \"--find_sites\"\n")
                        .build()
        );
        helpRunGroupOptions.setRequired(true);
        helpAndRunOptions.addOptionGroup(helpRunGroupOptions);
        helpAndRunOptions.addOptionGroup(createInfileRunOptions());
        return helpAndRunOptions;
    }

    /** Makes the --infile options which is then called by the function createHelpOrRunOptions.
     * @return The extra options that are made by the function to make all options.
     */
    private static OptionGroup createInfileRunOptions() {
        OptionGroup inFileRunOptions = new OptionGroup();
        String[] descriptionArray =
                {
                        "Returns nucleotide sequences of the genes that match the gene name pattern, in Fasta " +
                                "format.\nExample: user:java -jar GenBankReader-0.1.jar --infile " +
                                "example_data/genbank_file.gb --fetch_gene AXL2",
                        "Returns the amino acid sequences of the CDSs that match the product name pattern, in " +
                                "Fasta format.\nExample: user:java -jar GenBankReader-0.1.jar --infile " +
                                "data/example_genbank_file.gb --fetch_cds Rev7p",
                        "Returns all features with name, type, start, stop and orientation between the given " +
                                "coordinates. Coordinates are given as from..to. Only features that are completely " +
                                "covered on the given region should be listed.\nExample: user:java -jar " +
                                "GenBankReader-0.1.jar --infile data/Haloarcula_marismortui_genome.gb " +
                                "--fetch_features 5000..10000",
                        "Lists the locations of all the sites where the DNA pattern is found: position and actual" +
                                " sequence and (if relevant) the gene in which it resides.\nExample: user:java -jar " +
                                "GenBankReader-0.1.jar --infile data/example_genbank_file.gb --find_sites AAARTTT"
                };
        inFileRunOptions.addOption(
                Option.builder(null)
                        .longOpt("summary")
                        .hasArg(false)
                        .desc("Creates a textual summary of the parsed file: parsed file, length of the sequence," +
                                " and for genes: count and forward/reverse balance and for CDS features: count only." +
                                " NB: the F/R balance is the number on the forward strand divided by the total " +
                                "number.\nExample: user:java -jar GenBankReader-0.1.jar --infile " +
                                "data/example_genbank_file.gb --summary")
                        .build()
        );
        for (int i = 0; i < 4; i++) {
            inFileRunOptions.addOption(
                    Option.builder(null)
                            .hasArg(true)
                            .numberOfArgs(1)
                            .longOpt(longOptionLinkedList.get(i))
                            .desc(descriptionArray[i])
                            .build()
            );
        }
        inFileRunOptions.setRequired(true);
        return inFileRunOptions;
    }

    /** Makes the selected option available which makes it possible to get different results.
     * @return String of the selected option.
     */
    public String getSelectedOption() {
        return selectedOption;
    }

    /** Returns the filename which should be processed.
     * @return String version of filename.
     */
    public String getInfileOption() {
        return infileOption;
    }

    /** Makes the value specified for the selected option available.
     * @return String of the value.
     */
    public String getSelectedOptionInputValue() {
        return selectedOptionInputValue;
    }

    /** The results of a test if the specified information of the option was correct.
     * @return Boolean if the input was correct.
     */
    public boolean isInputIsCorrect() {
        return inputIsCorrect;
    }
}
