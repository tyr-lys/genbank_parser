package specifier;

import models.*;

import java.io.*;

public class ModelSpecifier {

    /** Determines which option should be chosen based upon the processOption.
     * @param genBankFile String of the given GenBank file to make a BufferedReader.
     * @param chosenProcessOption String of the chosen option to determine what must be done.
     * @param chosenInputValue String of the value that can be used to acquire certain results for an option.
     */
    public ModelSpecifier(String genBankFile, String chosenProcessOption, String chosenInputValue) {
        BufferedReader reader = GenBankBufferedReader(genBankFile);
        if (reader == null){
            System.out.printf("File or Path to %s is not found, please fill in a correct name in next time.\n", genBankFile);
        } else if (chosenProcessOption.equals("summary")) {
            Summary newSum = new Summary(genBankFile, reader);
            System.out.println(newSum);
        } else if(chosenProcessOption.equals("fetch_gene")){
            // test function
            Genes newGenes = new Genes(reader, chosenInputValue, "gene","gene");
            System.out.println(newGenes);
        } else if(chosenProcessOption.equals("fetch_cds")){
            Cds newCds = new Cds(reader,chosenInputValue,"product","CDS");
            System.out.println(newCds);
        } else if (chosenProcessOption.equals("fetch_features")){
            Features newFeatures = new Features(reader,chosenInputValue);
            System.out.println(newFeatures);
        } else if (chosenProcessOption.equals("find_sites")){
            Sites newSites = new Sites(reader, chosenInputValue);
            System.out.println(newSites);
        }
    }

    /** Makes a reader object that is used for reading GenBank files.
     * @param genBankFile String that represents the name of the genBankFile.
     * @return BufferedReader if the file is found else null which will result in a message of a missing file.
     */
    private BufferedReader GenBankBufferedReader(String genBankFile) {
        try {
            FileInputStream fileStream = new FileInputStream(genBankFile);
            DataInputStream fileInputStream = new DataInputStream(fileStream);
            return new BufferedReader(new InputStreamReader(fileInputStream));
        } catch (FileNotFoundException e){
            return null;
        }
    }


}

