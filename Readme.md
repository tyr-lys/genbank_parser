# GenBankReader

###### Author: Tyr-Lys

## What is the GenBankReader?
The GenBankReader is a tool to acquire information about a GenBank file. It has several options to describe the contents of a GenBank.
With the Summary use case it is possible to collect general information about the contents of the GenBank file.
The option fetch_gene gives the possibility to search the GenBank file for a gene with a certain name based on a regex.
The option fetch_cds does the same a fetch_gene but than specifically for the CDS tag.
Genes and CDS's with their names can also be found with the option fetch_features if a certain range is specified.
Another option within this tool is to specify a DNA string based on IUPAC characters.  

##Requirements
[Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)

## Examples with results.
######All of these examples are modified versions of choice_1_GenBankReader.md owned by Michiel Noback found at [this](https://bitbucket.org/tyr-lys/javaintroprogrammingassignments/src/master/src/main/java/week5_final_assignments/choice_1_GenBankReader.md) repository.
######Copyright © (2018) [Michiel Noback](michiel.noback@gmail.com) All rights reserved.

##### Summary use case.
```
user: java -jar GenBankReader-0.1.jar --infile data/example_genbank_file.gb --summary  
file              data/example_genbank_file.gb  
organism          Saccharomyces cerevisiae  
accession         U49845  
sequence length   5028 bp  
number of genes   2  
gene F/R balance  0.5  
number of CDSs    3  
```


##### fetch_gene use case.
```
user: java -jar GenBankReader-0.1.jar --infile example_data/genbank_file.gb --fetch_gene AXL2
>gene AXL2 sequence  
atgacacagcttcagatttcattattgctgacagctactatatcactactccatctagtagtggccacgccctatgaggc  
atatcctatcggaaaacaataccccccagtggcaagagtcaatgaatcgtttacatttcaaatttccaatgatacctata  
aatcgtctgtagacaagacagctcaaataacatacaattgcttcgacttaccgagctggctttcgtttgactctagttct  
agaacgttctcaggtgaaccttcttctgacttactatctgatgcgaacaccacgttgtatttcaatgtaatactcgaggg  
tacggactctgccgacagcacgtctttgaacaatacataccaatttgttgttacaaaccgtccatccatctcgctatcgt  
cagatttcaatctattggcgttgttaaaaaactatggttatactaacggcaaaaacgctctgaaactagatcctaatgaa  
gtcttcaacgtgacttttgaccgttcaatgttcactaacgaagaatccattgtgtcgtattacggacgttctcagttgta  
taatgcgccgttacccaattggctgttcttcgattctggcgagttgaagtttactgggacggcaccggtgataaactcgg  
cgattgctccagaaacaagctacagttttgtcatcatcgctacagacattgaaggattttctgccgttgaggtagaattc  
gaattagtcatcggggctcaccagttaactacctctattcaaaatagtttgataatcaacgttactgacacaggtaacgt  
ttcatatgacttacctctaaactatgtttatctcgatgacgatcctatttcttctgataaattgggttctataaacttat  
tggatgctccagactgggtggcattagataatgctaccatttccgggtctgtcccagatgaattactcggtaagaactcc  
aatcctgccaatttttctgtgtccatttatgatacttatggtgatgtgatttatttcaacttcgaagttgtctccacaac  
ggatttgtttgccattagttctcttcccaatattaacgctacaaggggtgaatggttctcctactattttttgccttctc  
agtttacagactacgtgaatacaaacgtttcattagagtttactaattcaagccaagaccatgactgggtgaaattccaa  
tcatctaatttaacattagctggagaagtgcccaagaatttcgacaagctttcattaggtttgaaagcgaaccaaggttc  
acaatctcaagagctatattttaacatcattggcatggattcaaagataactcactcaaaccacagtgcgaatgcaacgt  
ccacaagaagttctcaccactccacctcaacaagttcttacacatcttctacttacactgcaaaaatttcttctacctcc  
gctgctgctacttcttctgctccagcagcgctgccagcagccaataaaacttcatctcacaataaaaaagcagtagcaat  
tgcgtgcggtgttgctatcccattaggcgttatcctagtagctctcatttgcttcctaatattctggagacgcagaaggg  
aaaatccagacgatgaaaacttaccgcatgctattagtggacctgatttgaataatcctgcaaataaaccaaatcaagaa  
aacgctacacctttgaacaacccctttgatgatgatgcttcctcgtacgatgatacttcaatagcaagaagattggctgc  
tttgaacactttgaaattggataaccactctgccactgaatctgatatttccagcgtggatgaaaagagagattctctat  
caggtatgaatacatacaatgatcagttccaatcccaaagtaaagaagaattattagcaaaacccccagtacagcctcca  
gagagcccgttctttgacccacagaataggtcttcttctgtgtatatggatagtgaaccagcagtaaataaatcctggcg  
atatactggcaacctgtcaccagtctctgatattgtcagagacagttacggatcacaaaaaactgttgatacagaaaaac  
ttttcgatttagaagcaccagagaaggaaaaacgtacgtcaagggatgtcactatgtcttcactggacccttggaacagc  
aatattagcccttctcccgtaagaaaatcagtaacaccatcaccatataacgtaacgaagcatcgtaaccgccacttaca  
aaatattcaagactctcaaagcggtaaaaacggaatcactcccacaacaatgtcaacttcatcttctgacgattttgttc  
cggttaaagatggtgaaaatttttgctgggtccatagcatggaaccagacagaagaccaagtaagaaaaggttagtagat  
ttttcaaataagagtaatgtcaatgttggtcaagttaaggacattcacggacgcatcccagaaatgctgtga  
```

##### fetch_cds use case
```
user: java -jar GenBankReader-0.1.jar --infile data/example_genbank_file.gb --fetch_cds Rev7p
>CDS REV7 sequence  
MNRWVEKWLRVYLKCYINLILFYRNVYPPQSFDYTTYQSFNLPQFVPINRHPALIDYIEELILDVLSKLTHVYRFSICII  
NKKNDLCIEKYVLDFSELQHVDKDDQIITETEVFDEFRSSLNSLIMHLEKLPKVNDDTITFEAVINAIELELGHKLDRNR  
RVDSLEEKAEIERDSNWVKCQEDENLPDNNGFQPPKIKLTSLVGSDVGPLIIHQFSEKLISGDDKILNGVYSQYEEGESI  
FGSLF  
```

##### fetch_features use case
```
user: java -jar GenBankReader-0.1.jar --infile data/Haloarcula_marismortui_genome.gb --fetch_features "5000..10000"  
FEATURE;TYPE;START;STOP;ORIENTATION  
rrnB0003;gene;6187;6450;F  
hypothetical protein;CDS;6187;6450;F  
cdc6b;gene;6826;8064;R  
cell division control protein 6 homolog 1;CDS;6826;8064;R  
rrnB0005;gene;9123;9221;F  
hypothetical protein;CDS;9123;9221;F
```

##### find_sites use case
```
user: java -jar GenBankReader-0.1.jar --infile data/example_genbank_file.gb --find_sites AAARTTT 
site search: AAARTTT (regex: AAA[AG]TTT)
POSITION;SEQUENCE;GENE  
2109;AAAATTT;AXL2  
3022;AAAATTT;AXL2  
3358;AAAATTT;REV7  
4138;AAAGTTT;INTERGENIC 
```
